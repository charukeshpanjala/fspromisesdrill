const fs = require("fs")
const path = require("path")

const readFile = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf8", (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    })
}


const convertToUpperCaseAndCreateFile = (data, fileName) => {
    return new Promise((resolve, reject) => {
        const uppercaseData = data.toUpperCase()
        fs.writeFile(fileName, uppercaseData, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`${fileName} created.`)
                fs.appendFile("./filenames.txt", fileName + ", ", (err) => {
                    if (err) {
                        reject(err)
                    } else {
                        console.log(`${fileName} added to filenames.txt`)
                        resolve(fileName)
                    }
                })
            }
        })
    })
}

const convertToLowerCaseAndSplit = (fileName, newfileName) => {
    return new Promise((resolve, reject) => {
        const filePath = path.join(__dirname, fileName)
        readFile(filePath).then((res) => {
            let data = res.toLowerCase().replaceAll(". ", "\n")
            fs.writeFile(newfileName, data, (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${newfileName} created.`)
                    fs.appendFile("./filenames.txt", newfileName + ", ", (err) => {
                        if (err) {
                            reject(err)
                        } else {
                            console.log(`${newfileName} added to filenames.txt`)
                            resolve(newfileName)
                        }
                    })
                }
            })
        })
    })
}

const sortData = (fileName, newfileName) => {
    return new Promise((resolve, reject) => {
        const filePath = path.join(__dirname, fileName)
        readFile(filePath).then((res) => {
            const data = res.split("\n").sort()
            const sortedData = data.reduce((acc, cuu) => acc + ". " + cuu)
            fs.writeFile(newfileName, sortedData, (err) => {
                if (err) {
                    reject(err)
                } else {
                    console.log(`${newfileName} created.`)
                    fs.appendFile("./filenames.txt", newfileName, (err) => {
                        if (err) {
                            reject(err)
                        } else {
                            console.log(`${newfileName} added to filenames.txt`)
                            resolve()
                        }
                    })
                }
            })
        })
    })
}

const deleteData = () => {
    return new Promise((resolve, reject) => {
        readFile("./filenames.txt").then((res) => {
            const filenames = res.split(", ")
            let count = 0
            filenames.forEach(element => {
                const filePath = path.resolve(__dirname, element)
                fs.unlink(filePath, (err) => {
                    if (err) {
                        reject(err)
                    } else {
                        count += 1
                        if (count === filenames.length) {
                            fs.writeFile("./filenames.txt", "", (err) => {
                                if (err) {
                                    reject(err)
                                }
                            })
                            resolve("All files deleted")
                        }
                    }
                })
            });
        })
    })
}



// readFile("./lipsum.txt")
//     .then((res) => convertToUpperCaseAndCreateFile(res, "uppercaseData.txt"))
//     .then((res) => convertToLowerCaseAndSplit(res, "lowercaseSplitData.txt"))
//     .then((res) => sortData(res, "sortedData.txt"))
//     .then(() => deleteData("./filenames.txt"))
//     .then((res) => console.log(res))
//     .catch((rej) => console.log(rej))


//using async await functions
async function output() {
    const file = await readFile("./lipsum.txt")
    const path = await convertToUpperCaseAndCreateFile(file, "uppercaseData.txt")
    const newPath = await convertToLowerCaseAndSplit(path, "lowercaseSplitData.txt")
    const final = await sortData(newPath, "sortedData.txt")
    const result = await deleteData("./filenames.txt")
    console.log(result)
}

output()