const fs = require("fs")

const createDirectory = (path) => {
    return new Promise((resolve, reject) => {
        fs.mkdir(path, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`${path} directory created`)
                resolve(path)
            }
        })
    })
}
const createFiles = (path, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(`./${path}/${data}.json`, data, "utf8", (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`${data}.json file created`)
                resolve(path)
            }
        })
    })
}
const getAllFileNames = (path) => {
    return new Promise((resolve, reject) => {
        fs.readdir(`./${path}`, (err, data) => {
            if (err) {
                console.log(err)
            } else {
                resolve(data)
            }
        })

    })
}

const deleteAllFiles = (path) => {
    return new Promise((resolve, reject) => {
        getAllFileNames(path).then((res) => {
            let count = 0
            res.map(Element => {
                fs.unlink(`./${path}/${Element}`, (err) => {
                    if (err) {
                        console.log(err)
                    } else {
                        count += 1
                        if (count === res.length) {
                            fs.rmdir(`./${path}`, (err) => {
                                if (err) {
                                    console.log(err)
                                } else {
                                    resolve("All Files and Directory deleted")
                                }
                            })
                        }
                    }
                })
            })
        }
        )
    })
}

// createDirectory("jsonFiles")
//     .then((res) => createFiles(res, "one"))
//     .then((res) => createFiles(res, "two"))
//     .then((res) => createFiles(res, "three"))
//     .then((res) => createFiles(res, "four"))
//     .then((res) => deleteAllFiles(res))
//     .then((res) => console.log(res))
//     .catch((rej) => console.log(rej))

// using async await functions

async function result() {
    let path = await createDirectory("jsonFiles")
    path = await createFiles(path, "one")
    path = await createFiles(res, "two")
    path = await createFiles(res, "three")
    path = await createFiles(res, "four")
    const res = await deleteAllFiles(res)
    console.log(res)
}

result()